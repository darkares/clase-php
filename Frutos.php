<?php

/*

 */

/**
 * Description of Frutos
 *
 * @author User
 */
 include './arbol.php';
class Frutos extends Arbol
{
    private $color;
    private $peso;
    private $textura;
    private $sabor;
    private $volumen;
    
    public function __construct() {
        parent::__construct();
        $this->color ="amarilla";
        $this->peso ="0";
        $this->textura ="";
        $this->sabor ="";
        $this->volumen ="";
    }
    
    public function __destruct() {
        parent::__destruct();
        echo "<br><b>Hasta pronto</b>";
    }
            
    
    function getColor() {
        return $this->color;
    }

    function getPeso() {
        return $this->peso;
    }

    function getTextura() {
        return $this->textura;
    }

    function getSabor() {
        return $this->sabor;
    }
    function getVolumen() {
        return $this->volumen;
    }

    function setVolumen($volumen) {
        $this->volumen = $volumen;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setPeso($peso) {
        $this->peso = $peso;
    }

    function setTextura($textura) {
        $this->textura = $textura;
    }

    function setSabor($sabor) {
        $this->sabor = $sabor;
    }
    
      public function  saborfruta($sabor)
    {
         switch ($sabor){
             case 1:
                 $this->sabor ="Dulce";
                 $this->setFamilia("ddddddd");
                 break;
              case 2:
                 $this->sabor ="Ácido";
                   $this->setFamilia("zzzzz");
                 break;
              case 3:
                 $this->sabor ="Ácido";
                   $this->setFamilia("yyyyyy");
                 break;
              case 4:
                 $this->sabor ="Simple";
                   $this->setFamilia("xxxxx");
                 break;    
        }
    }
    public function volumenfruta($peso) {
        if ($peso<=0.2){
            $this->volumen= "Pequeña";
        }
            elseif($peso>0.2 and $peso<=1){
                $this->volumen= "Mediana";
            }
            else{
                $this->volumen= "Grande";
            }

    }


}

